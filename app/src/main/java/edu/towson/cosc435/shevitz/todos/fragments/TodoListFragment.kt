package edu.towson.cosc435.shevitz.todos.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import edu.towson.cosc435.shevitz.todos.Adapter.TodoAdapter
import edu.towson.cosc435.shevitz.todos.R
import edu.towson.cosc435.shevitz.todos.interfaces.ITodosController
import kotlinx.android.synthetic.main.todolist_fragment.*

class TodoListFragment: Fragment() {

    private lateinit var todoController: ITodosController

    override fun onAttach(context: Context) {
        super.onAttach(context)
        when(context) {
            is ITodosController -> todoController = context
            else -> throw Exception("context should have implemented IToDoController")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.todolist_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        add_todo_button.setOnClickListener { todoController.launchAddScreen() }

        val adapter = TodoAdapter(todoController)

        recyclerView.adapter = adapter

        recyclerView.layoutManager = LinearLayoutManager(view.context)
    }
}