package edu.towson.cosc435.shevitz.todos.Database

import edu.towson.cosc435.shevitz.todos.models.ToDoList
import androidx.room.*

import java.util.*


@Dao
interface TodoDao {
    @Insert
    fun addToDo(toDoList: ToDoList)

    @Update
    fun updateToDo(toDoList: ToDoList)

    @Delete
    fun deleteToDo(toDoList: ToDoList)

    @Query("select id, title, contents, complete, date, image from todo")
    fun getToDoList():List<ToDoList>
}

class DateConverter{
    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return if(value == null) null
        else Date(value)
    }
    @TypeConverter
    fun toTimestamp(date: Date?): Long? {
        return date?.time
    }
}

class UUIDconverter{
    @TypeConverter
    fun fromString(uuid: String): UUID {
        return UUID.fromString(uuid)
    }
    @TypeConverter
    fun toString(uuid: UUID): String {
        return uuid.toString()
    }
}

@Database(entities = [ToDoList::class], version = 2, exportSchema = false)
@TypeConverters(UUIDconverter::class,DateConverter::class)
abstract class TodoDatabase : RoomDatabase() {
    abstract fun todoDao(): TodoDao
}
