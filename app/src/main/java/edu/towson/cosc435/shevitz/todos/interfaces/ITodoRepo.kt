package edu.towson.cosc435.shevitz.todos.interfaces

import edu.towson.cosc435.shevitz.todos.models.ToDoList


interface ITodoRepo {
    fun getTodos(): List<ToDoList>
    fun getTodo (idx:Int): ToDoList
    fun dateMade(idx:Int): Long
    suspend fun updateToDo(Idx: Int, toDoList: ToDoList)
    suspend fun deleteToDo(idx: Int)
    fun getCount(): Int
    fun addTodo(todo: ToDoList)
}