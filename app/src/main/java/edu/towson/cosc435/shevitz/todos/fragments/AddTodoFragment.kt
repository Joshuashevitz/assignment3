package edu.towson.cosc435.shevitz.todos.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import edu.towson.cosc435.shevitz.todos.R
import edu.towson.cosc435.shevitz.todos.interfaces.ITodosController
import edu.towson.cosc435.shevitz.todos.models.ToDoList
import either.Either
import kotlinx.android.synthetic.main.fragment_add_todo.*
import kotlinx.android.synthetic.main.fragment_add_todo.Contents
import kotlinx.android.synthetic.main.fragment_add_todo.Title
import kotlinx.android.synthetic.main.fragment_add_todo.Completed
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*


class AddTodoFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?  {
        return inflater.inflate(R.layout.activity_new_todo, container, false)
    }

    private lateinit var todoController : ITodosController
    private var editTodoId: UUID? = null
    private var prevDate: Long? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        when(context) {
            is ITodosController -> todoController = context
            else -> throw Exception("context didn't implement Interface for TodoController")
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        populateForm()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        savebtn.setOnClickListener {
            when(val todo = getTodoFromView()) {
                is Either.Left -> {
                    errorM.text = todo.value
                    errorM.visibility = View.VISIBLE
                }
                is Either.Right ->{
                   todoController.runAsync{
                       try {
                           todoController.addNewTodo(todo.value)
                           populateForm()
                       } catch (e: Exception) {

                       }
                   }
                }
            }
        }
    }


    private fun getTodoFromView(): Either<String, ToDoList> {
        val date = Date().time
//        val df = SimpleDateFormat("yyyy.MM.dd HH:mm")

        val title = Title.editableText.toString()
        val contents = Contents.editableText.toString()
        val complete = Completed.isChecked
        var dateMade = prevDate

        if (title.isEmpty()) return Either.Left("You Must Add a Title!")
        if (contents.isEmpty()) return Either.Left("You Need To Add Some Content Descriptor!")

        val id= if(editTodoId == null)
        {
            UUID.randomUUID()
        } else {
            editTodoId!!
        }
        if (dateMade == null) dateMade = date
        return Either.Right(
            ToDoList(
            id = id,
            title = title,
            completed = complete,
            contents = contents,
            dateMade = dateMade,
            image = ""
        ))
    }
    private fun populateForm() {
        val todo = todoController.grabEditableTodo()
        Title.editableText.clear()
        Contents.editableText.clear()
        Completed.isChecked = false

        if(todo != null){
            editTodoId = todo.id
            Title.editableText.append(todo.title)
            Contents.editableText.append(todo.contents)
            Completed.isChecked=todo.completed
            savebtn.text="Edit Todo"
            savebtn.text = "Add Todo"
        }
    }
}