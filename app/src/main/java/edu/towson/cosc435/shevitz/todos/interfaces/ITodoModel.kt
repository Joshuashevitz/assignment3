package edu.towson.cosc435.shevitz.todos.interfaces

import edu.towson.cosc435.shevitz.todos.models.ToDoList

interface ITodoModel {
    fun getTodos():List<ToDoList>
    fun getTodo(pos: Int): ToDoList
    suspend fun deleteTodo(pos:Int)
    suspend fun markCompleted(pos:Int)
    suspend fun addTodo(todo:ToDoList)
    fun editTodo(pos:Int): ToDoList
    fun clearAndFill()
    fun fetchTodoCount():Int
}