package edu.towson.cosc435.shevitz.todos.models

import edu.towson.cosc435.shevitz.todos.interfaces.ITodoModel
import edu.towson.cosc435.shevitz.todos.interfaces.ITodoRepo

class ToDoModelS(private val repo : ITodoRepo) :ITodoModel {
    private var editing = false
    private var editingPos = -1

    override fun getTodos(): List<ToDoList> {
        return repo.getTodos()
    }

    override fun getTodo(pos: Int): ToDoList {
        return repo.getTodo(pos)
    }


    override suspend fun deleteTodo(pos: Int) {
        repo.deleteToDo(pos)
    }
    override suspend fun markCompleted(pos:Int) {
        val todo = repo.getTodo(pos)
        todo.completed = !todo.completed
        repo.updateToDo(pos,todo)
    }

    override suspend fun addTodo(todo:ToDoList) {
        if(editing) {
            repo.updateToDo(editingPos, todo)
            clearAndFill()

        }else {
            repo.addTodo(todo)
        }
    }

    override fun editTodo(pos: Int): ToDoList {
        editingPos = pos
        editing = true
        return repo.getTodo(pos)
    }

    override fun clearAndFill() {
        editingPos = -1
        editing = false
    }

    override fun fetchTodoCount(): Int {
        return repo.getCount()
    }

}