package edu.towson.cosc435.shevitz.todos.network

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import edu.towson.cosc435.shevitz.todos.interfaces.ITodosController
import kotlinx.coroutines.Dispatchers
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONArray
import java.util.*
import edu.towson.cosc435.shevitz.todos.models.ToDoList
import kotlinx.coroutines.withContext

interface ItodoAPI {
    suspend fun fetchTodos(): List<ToDoList>
    suspend fun fetchImg(imgUrl: String): Bitmap?
}

class TodoAPI(val controller:ITodosController): ItodoAPI {
    private val BASE_URL:String ="https://my-json-server.typicode.com/rvalis-towson/todos_api/todos"


  override suspend fun fetchTodos(): List<ToDoList>{
       return withContext(Dispatchers.IO) {
           val client = OkHttpClient()
           val request = Request.Builder()
               .url(BASE_URL)
               .get()
               .build()
           val response = client.newCall(request).execute()
           if (response.isSuccessful) {
               val todoJson = response.body?.string()
               val result = object: TypeToken<List<ToDoList>>(){}.type
               val todos = Gson().fromJson<List<ToDoList>> (todoJson, result)
               todos
           } else
               listOf()
       }
   }

  override suspend fun fetchImg(imgUrl: String): Bitmap? {
       return withContext(Dispatchers.IO) {
           val filename = getImageFilename(imgUrl)
           val bitmap = controller.checkCache(filename)
           if(bitmap != null) {
               bitmap
           } else {
               val client = OkHttpClient()
               val request = Request.Builder()
                   .url(imgUrl)
                   .get()
                   .build()
               val stream = client.newCall(request).execute()
               if(stream.isSuccessful){
                   val bStream = stream.body?.byteStream()
                   val bitmap = BitmapFactory.decodeStream(bStream)
                   bitmap
               }else {
                   null
               }
           }
       }
    }
//    private fun parseJson(json:String?): List<ToDoList> {
//        val todos = mutableListOf<ToDoList>()
//        if(json == null) return todos
//        val jsonarray = JSONArray(json)
//        var i = 0
//        while(i<jsonarray.length()) {
//            val jsonObj = jsonarray.getJSONObject(i)
//            val todo = ToDoList (
//                id = UUID.fromString(jsonObj.getString("id")),
//                title = jsonObj.getString("title "),
//                contents = jsonObj.getString("contents"),
//                completed = jsonObj.getBoolean("created"),
//                dateMade = jsonObj.getLong("date made"),
//                image = jsonObj.getString("image")
//                )
//            todos.add(todo)
//            i++
//        }
//        return todos
//    }


    private fun getImageFilename(url: String): String {
        val urlObj = java.net.URL(url)
        val query = urlObj.query
        val filename = query.replace("=", "")
        return filename.plus(".jpg")
    }
}