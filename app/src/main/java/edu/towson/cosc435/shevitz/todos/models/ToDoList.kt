package edu.towson.cosc435.shevitz.todos.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "todo")
data class ToDoList(
    @PrimaryKey
    val id: UUID,
    @ColumnInfo(name="title")
    var title: String,
    @ColumnInfo(name="contents")
    var contents: String,
    @ColumnInfo (name="complete")
    @SerializedName("completed")
    var completed: Boolean,
    @ColumnInfo(name="date")
    var dateMade: Long,
    @ColumnInfo(name ="image")
    @SerializedName("image")
    var image: String
)