package edu.towson.cosc435.shevitz.todos

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import edu.towson.cosc435.shevitz.todos.Database.TodoRepoDatabase
import edu.towson.cosc435.shevitz.todos.interfaces.ITodoModel
import edu.towson.cosc435.shevitz.todos.interfaces.ITodosController
import edu.towson.cosc435.shevitz.todos.models.ToDoList
import edu.towson.cosc435.shevitz.todos.models.ToDoModelS
import edu.towson.cosc435.shevitz.todos.network.ItodoAPI
import edu.towson.cosc435.shevitz.todos.network.TodoAPI
import kotlinx.coroutines.launch
import kotlinx.coroutines.*
import java.io.File

class MainActivity : AppCompatActivity(),ITodosController {


    private lateinit var todoModel: ITodoModel
    private var editingTodoPos = -1
    private lateinit var todoAPI: ItodoAPI
    private val scope = CoroutineScope(Dispatchers.Main)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


            todoAPI = TodoAPI(this)
            todoModel = ToDoModelS(TodoRepoDatabase(applicationContext))

        scope.launch {
            val todoList = todoAPI.fetchTodos()
            val todoListDB = todoModel.getTodos()
            todoList.forEach{ todoAPI ->
                if (todoListDB.firstOrNull{todoDB -> todoDB.id == todoAPI.id} == null){
                    todoModel.addTodo(todoAPI)
                }

            }
        }
    }

    override suspend fun deleteTodo(pos: Int) {

        try{
           withContext(Dispatchers.Main){
               todoModel.deleteTodo(pos)
        }
        }catch (e:Exception){
            Toast.makeText(this@MainActivity, "failed to delete todo item", Toast.LENGTH_SHORT)
                .show()
            throw e
        }

    }

    override fun editTodo(pos: Int) {
        editingIdx = pos
       findNavController(R.id.nav_host_fragment).navigate(R.id.action_todoListFragment_to_addTodoFragment)
    }

    override fun fetchToDoCount(): Int {
        return todoModel.fetchTodoCount()
    }

    override suspend fun markCompleted(pos: Int) {
        try{
            withContext(Dispatchers.IO){
                todoModel.markCompleted(pos)
            }
        }catch (e:Exception) {
            Toast.makeText(this, "Failed to mark as completed", Toast.LENGTH_SHORT).show()
        }

    }

    override fun getTodo(pos: Int):ToDoList {
     return todoModel.getTodo(pos)
    }

    override suspend fun getTodos(): List<ToDoList> {
        return todoModel.getTodos()
    }

    override suspend fun addNewTodo(toDoList: ToDoList) {
        withContext(Dispatchers.IO) {
            try {
                todoModel.addTodo(toDoList)
                editingIdx = -1
                todoModel.clearAndFill()
                findNavController(R.id.nav_host_fragment).popBackStack()
            } catch (e: Exception) {
                Toast.makeText(this@MainActivity, "Failed to add new Todo!", Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }

    override fun grabEditableTodo(): ToDoList? {
        if (editingTodoPos<0) return null
        return todoModel.editTodo(editingTodoPos)
    }

    override fun launchAddScreen() {
//        editingTodoPos = -1
//        todoModel.clearAndFill()
        findNavController(R.id.nav_host_fragment).navigate(R.id.action_todoListFragment_to_addTodoFragment)

    }

    override fun runAsync(blk: suspend () -> Unit) {
        scope.launch { blk() }
    }

    override fun checkCache(filename: String): Bitmap? {
        val file = File(cacheDir, filename)
        return if(file.exists()) {
            val input = file.inputStream()
            BitmapFactory.decodeStream(input)
        } else {
            null
        }
    }

    override fun cacheIcon(filename: String, img: Bitmap) {
        val file = File(cacheDir, filename)
        val output = file.outputStream()
        img.compress(Bitmap.CompressFormat.JPEG, 100, output)
    }


    override fun onStop() {
        super.onStop()
        scope.cancel()
    }


    private var editingIdx = -1

}
