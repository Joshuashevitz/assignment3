package edu.towson.cosc435.shevitz.todos.Adapter

import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import edu.towson.cosc435.shevitz.todos.R
import edu.towson.cosc435.shevitz.todos.interfaces.ITodosController
import edu.towson.cosc435.shevitz.todos.models.ToDoList
import edu.towson.cosc435.shevitz.todos.network.TodoAPI
import kotlinx.android.synthetic.main.todo_items_layouts.view.*
import java.lang.Exception


class TodoAdapter(private val controller: ITodosController) : RecyclerView.Adapter<TodoViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.todo_items_layouts,parent, false)
        val vh = TodoViewHolder(view)

        view.deleteBtn.setOnClickListener{
            val position=vh.adapterPosition
            controller.runAsync{
                try {
                    view.alpha = 0.5f
                    controller.deleteTodo(position)
                    this@TodoAdapter.notifyItemRemoved(position)
                }catch(e: Exception) {
                    view.alpha = 0.5f
                }
            }
        }

        view.checkbox.setOnClickListener {
            val position = vh.adapterPosition
            controller.runAsync{
                try {
                    view.alpha = 0.5f
                    controller.markCompleted(position)
                }catch(e: Exception) {

                } finally {
                    view.alpha = 1.0f
                }
            }

        }

        view.setOnClickListener {
            val position=vh.adapterPosition
            controller.runAsync {
                try {
                    view.alpha = 0.5f
                    controller.editTodo(position)
                    this@TodoAdapter.notifyItemRemoved(position)
                }catch(e: Exception) {
                    view.alpha = 0.5f
                }
            }

        }

        view.setOnLongClickListener {
            val position=vh.adapterPosition
            controller.runAsync {
                try {
                    view.alpha = 0.5f
                    controller.deleteTodo(position)

                    this@TodoAdapter.notifyItemRemoved(position)
                }catch(e: Exception) {
                    view.alpha = 0.5f
                }finally {

                }
            }
            true
        }

        return vh
    }

    override fun getItemCount(): Int {
        return controller.fetchToDoCount()
    }

    override fun onBindViewHolder(holder: TodoViewHolder, position: Int) {
        val todo = controller.getTodo(position)
        val tv = holder.itemView.todo_title
        updateStrikethrough(todo, tv)
        holder.onBind(controller,todo)
    }

    private fun updateStrikethrough(todo: ToDoList, tv: TextView) {
        if(todo.completed) {
            tv.paintFlags = tv.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        } else {
            tv.paintFlags = tv.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
        }
    }
}

class TodoViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun onBind(controller: ITodosController, todo: ToDoList) {
        val todosApi =TodoAPI(controller)
        itemView.todo_title.text = todo.title
        itemView.todo_text.text = todo.contents
        itemView.checkbox.isChecked = todo.completed
        itemView.todo_create_date.text = todo.dateMade.toString()
        itemView.image_icon.visibility = View.INVISIBLE
        controller.runAsync {
            val bitmap = todosApi.fetchImg(todo.image)
                itemView.image_icon.setImageBitmap(bitmap)
                itemView.image_icon.visibility = View.VISIBLE

        }
    }
}