package edu.towson.cosc435.shevitz.todos.Database

import android.content.Context
import androidx.room.Room
import edu.towson.cosc435.shevitz.todos.models.ToDoList
import edu.towson.cosc435.shevitz.todos.interfaces.ITodoRepo
import kotlinx.coroutines.delay


class TodoRepoDatabase(ctx: Context): ITodoRepo {
    private val DoList: MutableList<ToDoList> = mutableListOf()
    private val db: TodoDatabase = Room.databaseBuilder(
        ctx,
        TodoDatabase::class.java,
        "todo.db"
    ).build()



    override fun getTodo(idx: Int): ToDoList {
        return DoList[idx]
    }


    override fun dateMade(idx: Int): Long {
        return DoList[idx].dateMade
    }

    override suspend fun updateToDo(Idx: Int, todo: ToDoList) {
        delay(1600)
        if (System.currentTimeMillis() % 2 == 0L){
            clearTheList()
            throw Exception()
        }
        db.todoDao().updateToDo(todo)
    }

    override suspend fun deleteToDo(idx: Int) {
        delay(1600)
        if(System.currentTimeMillis()%2== 0L) throw Exception()
        val todoDelete = DoList[idx]
        db.todoDao().deleteToDo(todoDelete)
        clearTheList()
    }

    override fun getCount(): Int {
        return DoList.size
    }

    override fun addTodo(todo: ToDoList) {
        db.todoDao().addToDo(todo)
        clearTheList()
    }

    override fun getTodos(): List<ToDoList> {
        if(getCount() == 0){
            clearTheList()
        }
        return DoList
    }
    private fun clearTheList(){
        DoList.clear()
        DoList.addAll(db.todoDao().getToDoList())
    }

}