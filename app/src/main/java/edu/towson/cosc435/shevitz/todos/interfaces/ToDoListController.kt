package edu.towson.cosc435.shevitz.todos.interfaces

import android.graphics.Bitmap
import edu.towson.cosc435.shevitz.todos.models.ToDoList

    interface ITodosController  {
    fun editTodo(pos: Int)
    fun fetchToDoCount(): Int
    suspend fun deleteTodo(pos: Int)
    suspend fun markCompleted(pos:Int)
    fun getTodo(pos:Int): ToDoList
    suspend fun getTodos(): List<ToDoList>
    suspend fun addNewTodo(toDoList: ToDoList)
    fun grabEditableTodo():ToDoList?
    fun launchAddScreen()
    fun runAsync(blk: suspend () -> Unit)
    fun checkCache(filename: String): Bitmap?
    fun cacheIcon(filename: String, img: Bitmap)

}